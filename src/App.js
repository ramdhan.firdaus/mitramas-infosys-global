import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'
import DashboardPageComponent from './components/DashboardPageComponent';
import SidebarComponent from './/components/Bar/SidebarComponent';
import NavbarComponent from './components/Bar/NavbarComponent';
import TabelCustomerComponent from './components/TabelCustomerComponent';
import CreateCustomerComponent from './components/CreateCustomerComponent';
import UpdateCustomerComponent from './components/UpdateCustomerComponent';

function App() {
  return (
    <div className="App">
      <Router>
        <div className='row'>
          <div className='col-1 col-sm-3 side'>
            <SidebarComponent />
          </div> 
          <div className='col-11 col-sm-11'>
            <NavbarComponent />
            <Routes>
              <Route path="/" element={<DashboardPageComponent />}></Route>
              <Route path="/customers" element={<TabelCustomerComponent />}></Route>
              <Route path="/customers/create" element={<CreateCustomerComponent />}></Route>
              <Route path="/customers/update/:id" element={<UpdateCustomerComponent />}></Route>
            </Routes>
          </div>
        </div>
      </Router>
    </div>
  );
}

export default App;
