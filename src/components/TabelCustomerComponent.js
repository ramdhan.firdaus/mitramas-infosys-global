import React, { useState, useEffect } from 'react'
import axios from 'axios'
import url from '../config/url'
import token from '../config/Token'
import { MDBDataTableV5 } from 'mdbreact';
import { useNavigate } from 'react-router-dom';

const TabelCustomerComponent = () => {
  const [datatable, setDatatable] = useState({})
  const history = useNavigate()

  useEffect(() => {
    const getApi = async () => {
      await axios.get(`${url}/customers`, {
        headers: {
          Authorization: `${token}`
        },
      }).then((res) => {
        let array = []
        res.data.data.forEach(element => {
          element.status = element.status.toString().substring(0, 1).toUpperCase() + element.status.toString().substring(1)
          element.action = <div className='row'>
            <div className='col-xl-6 col-lg-12 col-md-12 mb-2'><button type="button" class="btn btn-warning" onClick={(e) => updateCustomer(element.id)}>Update</button></div>
            <div className='col-xl-6 col-lg-12 col-md-12 mb-2'><button type="button" class="btn btn-danger" onClick={(e) => deleteCustomer(element.id)}>Delete</button></div>
          </div>
          if (element.status === "True") {
            element.status = "Active"
          } else {
            element.status = "Inactive"
          }
          array.push(element)
        });
        setDatatable({
          columns: [
            {
              label: 'ID',
              field: 'id',
              sort: 'disabled',
              width: 50,
            },
            {
              label: 'Name',
              field: 'name',
              width: 100,
            },
            {
              label: 'Address',
              field: 'address',
              sort: 'disabled',
              width: 100,
            },
            {
              label: 'Country',
              field: 'country',
              sort: 'disabled',
              width: 100,
            },
            {
              label: 'Phone Number',
              field: 'phone_number',
              sort: 'disabled',
              width: 100,
            },
            {
              label: 'Job Title',
              field: 'job_title',
              sort: 'disabled',
              width: 100,
            },
            {
              label: 'Status',
              field: 'status',
              width: 50,
            },
            {
              label: 'Action',
              field: 'action',
              sort: 'disabled',
              width: 250,
            },
          ],
          rows: array
        })
      });
    }

    getApi()
  }, []);

  const updateCustomer = (id) => {
    history(`update/${id}`)
  }

  const deleteCustomer = (id) => {
    let data = {
      id: id
    }

    let req = {
      data,
      headers: {
        'Authorization': `${token}`
      }
    }

    axios.delete(`${url}/customers`, req)
    window.location.reload()
  }

  return (<>
    <div className='mx-2 my-3'><button type="button" class="btn btn-primary" onClick={() => history('create')}>Add Customer</button></div>
    <MDBDataTableV5
    button
      hover
      entriesOptions={[10, 25, 50]}
      entries={10}
      pagesAmount={4}
      data={datatable}
      pagingTop
      searchTop
      searchBottom={false}
    />
  </>
  )
}

export default TabelCustomerComponent