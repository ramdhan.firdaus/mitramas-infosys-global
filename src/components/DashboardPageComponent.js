import React from 'react';
import logo from '../assets/img/logo.jpg';

const DashboardPageComponent = () => {
  return (
    <div className='row'>
      <div className="col-sm-12 col-md-12 col-lg-3">
        <section className="content mb-3">
          <div className="card card-widget widget-user">
            <div className="widget-user-header text-white">
            </div>
            <div className="widget-user-image">
              <img className="img-circle" src={logo} alt="User Avatar" />
            </div>
            <div className="card-footer">
              <div className='mb-4'>
                <strong><h5 className='mt-2 text-center'>Mitramas Infosys Global</h5></strong>
                <h6 className='text-center'><small className='fw-lighter'>Layanan IT</small></h6>
              </div>
              <div className='mb-4'>
                <h6 className='mt-2 text-center sunting'><i className="bi bi-pencil me-2"></i>Sunting Profil</h6>
              </div>
              <div className='container'>
                <div className='mb-3'>
                  <h6><small className='fw-lighter'>Status Perusahaan</small></h6>
                  <div className='row'>
                    <div className='col-10'>
                      <h6 className='fw-normal status'><strong>Aktif</strong></h6>
                    </div>
                    <div className='col-1 text-end'>
                      <div className="form-switch">
                        <input className="form-check-input" type="checkbox" role="switch" id="flexSwitchCheckChecked" checked />
                      </div>
                    </div>
                  </div>
                </div>
                <div className='mb-3'>
                  <h6><small className='fw-lighter'>Singkatan</small></h6>
                  <h6 className='fw-normal'>MIG</h6>
                </div>
                <div className='mb-3'>
                  <h6><small className='fw-lighter'>Alamat Perusahaan</small></h6>
                  <h6 className='fw-normal'>Jl. Tebet Raya No.42, Jakarta Selatan</h6>
                </div>
                <div className='mb-3'>
                  <h6><small className='fw-lighter'>Penanggung Jawab (PIC)</small></h6>
                  <h6 className='fw-normal'>John Doe</h6>
                </div>
                <div className='mb-3'>
                  <h6><small className='fw-lighter'>Tanggal PKP</small></h6>
                  <h6 className='fw-normal'>03 Maret 2021</h6>
                </div>
                <div className='mb-3'>
                  <h6><small className='fw-lighter'>Email</small></h6>
                  <h6 className='fw-normal status'><i className="bi bi-envelope me-2"></i><a href='https://mail.google.com/mail/?view=cm&fs=1&to=mig@mitrasolusi.group'>mig@mitrasolusi.group</a></h6>
                </div>
                <div className='mb-3'>
                  <h6><small className='fw-lighter'>No Telp</small></h6>
                  <h6 className='fw-normal status'><i className="bi bi-telephone me-2"></i>021-5678-1234</h6>
                </div>
                <div className='mb-3'>
                  <h6><small className='fw-lighter'>Situs Web</small></h6>
                  <h6 className='fw-normal status'><i className="bi bi-globe2 me-2"></i><a href='http://mitramas.com/'>mitramas.com</a></h6>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
      <div className='col-lg-9'>
        <section className="content mb-3">
          <div className="card card-primary card-outline">
            <div className="card-body">
              <div className='row'>
                <div className='col-sm-2'>
                  <p><strong>LOKASI</strong></p>
                </div>
                <div className='col-7'></div>
                <div className='col-sm-3 text-end'>
                  <a href='/' className='all'>Lihat Semua</a>
                </div>
              </div>
              <div className="row">
                <div className="col-md-4 col-sm-6 col-12">
                  <div className="info-box text-end">
                    <span className="info-box-icon"><i className="far fa-envelope"></i></span>
                    <div className="info-box-content">
                      <span className="info-box-number">20</span>
                      <span className="info-box-text">Induk</span>
                    </div>
                  </div>
                </div>
                <div className="col-md-4 col-sm-6 col-12">
                  <div className="info-box text-end">
                    <span className="info-box-icon"><i className="far fa-envelope"></i></span>

                    <div className="info-box-content">
                      <span className="info-box-number">3</span>
                      <span className="info-box-text">Sub Lokasi Level 1</span>
                    </div>
                  </div>
                </div>
                <div className="col-md-4 col-sm-6 col-12">
                  <div className="info-box text-end">
                    <span className="info-box-icon"><i className="far fa-envelope"></i></span>

                    <div className="info-box-content">
                      <span className="info-box-number">1</span>
                      <span className="info-box-text">Sub Lokasi Level 2</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <div className='row'>
          <div className="col-lg-6">
            <section className="content mb-3">
              <div className="card card-primary card-outline">
                <div className="card-body">
                  <div className='row mb-3'>
                    <div className='col-sm-4'>
                      <p><strong>Akun Bank</strong></p>
                    </div>
                    <div className='col-2'></div>
                    <div className='col-sm-6 text-end'>
                      <button type="button" className="btn btn-successed btn-sm"><i className="bi bi-plus"></i>Tambah Akun Bank</button>
                    </div>
                  </div>
                  <div className='container'>
                    <div className='row mb-3'>
                      <div className='col-xl-3 col-lg-4 col-md-3 col-sm-3 visa-1 text-end mb-2'>
                        <p className='mt-5'>VISA</p>
                      </div>
                      <div className='col-xl-9 col-lg-12 col-md-9 col-sm-9'>
                        <div className='row'>
                          <div className='col-8'>
                            <p><strong>Bank KB Bukopin</strong></p>
                            <small>**** 0876 - Yusron Taufiq</small>
                            <br />
                            <small>IDR</small>
                          </div>
                          <div className='col-4 text-end'>
                            <i className="bi bi-pencil me-2"></i>
                            <i className="bi bi-trash3"></i>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className='row'>
                      <div className='col-xl-3 col-lg-4 col-md-3 col-sm-3 visa-2 text-end mb-2'>
                        <p className='mt-5'>VISA</p>
                      </div>
                      <div className='col-xl-9 col-lg-12 col-md-9 col-sm-9'>
                        <div className='row'>
                          <div className='col-8'>
                            <p><strong>Citibank,NA</strong></p>
                            <small>**** 5525 - Si Tampan</small>
                            <br />
                            <small>USD</small>
                          </div>
                          <div className='col-4 text-end'>
                            <i className="bi bi-pencil me-2"></i>
                            <i className="bi bi-trash3"></i>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
            <section className="content mb-3">
              <div className="card card-primary card-outline">
                <div className="card-body">
                  <div className='row mb-3'>
                    <div className='col-4'>
                      <p><strong>Relasi</strong></p>
                    </div>
                    <div className='col-4'></div>
                    <div className='col-4 text-end'>
                      <a href='/' className='all'>Lihat Semua</a>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-8">
                      <div className="info-box-relasi">
                        <span className="info-box-icon"><i className="bi bi-share"></i></span>
                        <div className="info-box-content">
                          <span className="info-box-number">20</span>
                          <small><span className="info-box-text fw-light">Memiliki</span></small>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-8">
                      <div className="info-box-relasi">
                        <span className="info-box-icon"><i className="bi bi-share"></i></span>
                        <div className="info-box-content">
                          <span className="info-box-number">108</span>
                          <small><span className="info-box-text fw-light">Menggunakan</span></small>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-8">
                      <div className="info-box-relasi">
                        <span className="info-box-icon"><i className="bi bi-share"></i></span>
                        <div className="info-box-content">
                          <span className="info-box-number">67</span>
                          <small><span className="info-box-text fw-light">Meminjam</span></small>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
          </div>
          <div className="col-lg-6">
            <section className="content mb-3">
              <div className="card card-primary card-outline">
                <div className="card-body aktivitas">
                  <div className='row'>
                    <div className='col-4'>
                      <p><strong>Aktivitas</strong></p>
                    </div>
                  </div>
                  <div className="rincian mb-2">
                    <p>Yusron baru saja menambahkan lokasi baru Kantor Cabang Jagakarsa</p>
                    <p className="fw-lighter"><small>Hari ini, 08:00</small></p>
                  </div>
                  <div className="rincian mb-2">
                    <p>Bintang baru saja menghapus sublokakasi KCP Tebet 4 dari induk Kantor Cabang Tebet</p>
                    <p className="fw-lighter"><small>Kemarin, 17:32</small></p>
                  </div>
                  <div className="rincian mb-2">
                    <p>Yusron melakukan perubahan profil pada induk Kantor Cabang Bekasi</p>
                    <p className="fw-lighter"><small>Kemarin, 17:32</small></p>
                  </div>
                </div>
              </div>
            </section>
          </div>
        </div>
      </div>
    </div>
  );
}

export default DashboardPageComponent