import axios from 'axios'
import React, { useState, useEffect } from 'react'
import url from '../config/url'
import token from '../config/Token'
import { useNavigate, useParams } from 'react-router-dom';

const UpdateCustomerComponent = () => {
  const { id } = useParams()
  const [name, setName] = useState('')
  const [address, setAddress] = useState('')
  const [country, setCountry] = useState('')
  const [pastCountry, setPastCountry] = useState('')
  const [phoneNumber, setPhoneNumber] = useState('')
  const [jobTitle, setJobTitle] = useState('')
  const [status, setStatus] = useState('')
  const [pastStatus, setPastStatus] = useState('')
  const [dataCountry, setDataCountry] = useState([])
  const history = useNavigate()

  useEffect(() => {
    const getApiCountry = async () => {
      let dataCountry = [{ value: "", label: "Your Country" }]
      await axios.get('https://countriesnow.space/api/v0.1/countries').then((res) => {
        res.data.data.forEach((element) => {
          dataCountry.push({ value: `${element.country}`, label: `${element.country}` })
        })
      })
      setDataCountry(dataCountry)
    }

    const getApi = async () => {
      await axios.get(`${url}/customers`, {
        headers: {
          Authorization: `${token}`
        },
      }).then((res) => {
        var BreakException = {};
        try {
          res.data.data.forEach(element => {
            if (element.id.toString() === id) {
              setName(element.name)
              setAddress(element.address)
              setCountry(element.country)
              setPastCountry(element.country)
              setPhoneNumber(element.phone_number)
              setJobTitle(element.job_title)
              setStatus(element.status)
              setPastStatus(element.status)
              throw BreakException;
            }
          });
        } catch (e) {
          if (e !== BreakException) throw e;
        }
      });
    }

    getApiCountry()
    getApi()
  }, [id]);

  const changeStatus = () => {
    if (status) {
      setStatus(false)
    } else {
      setStatus(true)
    }
  }

  const add = () => {
    let data = {
      id: id,
      name: name,
      address: address,
      country: country,
      phone_number: phoneNumber,
      job_title: jobTitle,
      status: status
    }

    let config = {
      headers: {
        'Authorization': `${token}`
      }
    }

    axios.put(`${url}/customers`, data, config)
    history('/customers')
  }

  return (
    <section className="vh-100">
      <div className="container py-5">
        <div className="row d-flex justify-content-center align-items-center h-100">
          <div className="col-12 col-md-10 col-lg-10 col-xl-10">
            <div className="card shadow-2-strong" style={{ borderRadius: "1rem" }}>
              <div className="card-body px-5 py-4 text-center">

                <h3 className="mb-3">Update Customer</h3>

                <div className="form-outline mb-1">
                  <label className="control-label d-flex p-2 ">Name :</label>
                  <div className="input-group">
                    <input type="text" className="form-control" placeholder='Your Name'
                      value={name} onChange={(e) => setName(e.target.value)} />
                  </div>
                </div>

                <div className="form-outline mb-1">
                  <label className="control-label d-flex p-2 ">Address :</label>
                  <div className="input-group">
                    <textarea type="text" className="form-control" placeholder='Your Address'
                      value={address} onChange={(e) => setAddress(e.target.value)} />
                  </div>
                </div>

                <div className="form-outline mb-1">
                  <label className="control-label d-flex p-2 ">Country : (Past={pastCountry})</label>
                  <div className="input-group">
                    <select className="form-select" onChange={(e) => setCountry(e.target.value)}>
                      {dataCountry.map((element, i) => (
                        <option key={`country-${i}`} value={element.value} label={element.label}></option>
                      ))}
                    </select>
                  </div>
                </div>

                <div className="form-outline mb-1">
                  <label className="control-label d-flex p-2 ">Phone Number :</label>
                  <div className="input-group">
                    <input type="text" className="form-control" placeholder='Your Phone Number'
                      value={phoneNumber} onChange={(e) => setPhoneNumber(e.target.value)} />
                  </div>
                </div>

                <div className="form-outline mb-3">
                  <label className="control-label d-flex p-2 ">Job Title :</label>
                  <div className="input-group">
                    <input type="text" className="form-control" placeholder='Your Job Title'
                      value={jobTitle} onChange={(e) => setJobTitle(e.target.value)} />
                  </div>
                </div>

                <div className="form-outline mb-3">
                  <label className="control-label d-flex p-2 ">Status : (Past={pastStatus.toString()})</label>
                  <div class="form-check text-start">
                    <input class="form-check-input" type="checkbox" role="switch" id="flexSwitchCheckDefault" onChange={() => changeStatus()} />
                    <label class="form-check-label" for="flexSwitchCheckDefault">Active or Not</label>
                  </div>
                </div>

                <button className="btn btn-primary btn-block mx-1" type="submit" onClick={() => history("/customers")}>Back</button>
                <button className="btn btn-primary btn-block mx-1" type="submit" onClick={add}>Update</button>

              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  )
}

export default UpdateCustomerComponent