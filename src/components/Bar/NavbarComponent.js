import React from 'react'
import pict from '../../assets/img/john.jpg'

const NavbarComponent = () => {
    return (<nav className="navbar navbar-expand-lg">
        <div className="container-fluid">
            <a className="navbar-brand" href="/">Perusahaan</a>>
            <div className="collapse navbar-collapse" id="navbarSupportedContent">
                <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                    <li className="nav-item">
                        <a className="nav-link name" aria-current="page" href="/"><strong>Mitramas Infosys Global</strong></a>
                    </li>
                </ul>
                <div>
                    <i className="bi bi-search me-2"></i>
                    <i className="bi bi-bell me-5"></i>
                    <img src={pict}
                   alt="Profile" className="picture me-2"></img>
                   <small><strong>John Doe</strong></small>
                </div>
            </div>
        </div>
    </nav>
    )
}

export default NavbarComponent