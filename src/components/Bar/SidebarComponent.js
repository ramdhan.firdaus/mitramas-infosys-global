import React from 'react';
import {
    CDBSidebar,
    CDBSidebarContent,
    CDBSidebarHeader,
    CDBSidebarMenu,
    CDBSidebarMenuItem,
} from 'cdbreact';
import { NavLink } from 'react-router-dom';

const SidebarComponent = () => {
    return (
        <div className='side-bar'>
            <CDBSidebar textColor="#333" backgroundColor="#ffff">
                <CDBSidebarHeader prefix={<img src={'https://static.wixstatic.com/media/e817ec_be43f247d0d4454f9d29e2d22f8d4ff7~mv2.png/v1/fill/w_243,h_94,al_c,q_85,usm_0.66_1.00_0.01,enc_auto/mig.png'} alt="" style={{ width: '30px' }} />}>
                    <div className="container text-center" >
                        <h6>Mitramas</h6>
                        <h6>Infosys Global</h6>
                    </div>
                </CDBSidebarHeader>
                <CDBSidebarContent>
                    <CDBSidebarMenu>
                        <NavLink exact to="/">
                            <CDBSidebarMenuItem icon="th-large">Dashboard</CDBSidebarMenuItem>
                        </NavLink>
                        <NavLink exact to="/customers">
                            <CDBSidebarMenuItem icon="clipboard-list">Customers</CDBSidebarMenuItem>
                        </NavLink>
                    </CDBSidebarMenu>
                </CDBSidebarContent>
            </CDBSidebar>
        </div>
    )
}

export default SidebarComponent